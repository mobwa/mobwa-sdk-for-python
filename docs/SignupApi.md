# mobwa.SignupApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**signup**](SignupApi.md#signup) | **POST** /signup | Signup


# **signup**
> SignUpResponse signup()

Signup

### Example

```python
import time
import mobwa
from mobwa.api import signup_api
from mobwa.model.sign_up_request import SignUpRequest
from mobwa.model.sign_up_response import SignUpResponse
from mobwa.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with mobwa.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = signup_api.SignupApi(api_client)
    sign_up_request = SignUpRequest(
        username="username_example",
        email="email_example",
        password="password_example",
    ) # SignUpRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Signup
        api_response = api_instance.signup(sign_up_request=sign_up_request)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling SignupApi->signup: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sign_up_request** | [**SignUpRequest**](SignUpRequest.md)|  | [optional]

### Return type

[**SignUpResponse**](SignUpResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Information related to the enrolled user |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

