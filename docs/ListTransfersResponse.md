# ListTransfersResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | [**[Transfer]**](Transfer.md) |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


