# mobwa.TransfersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**complete_transfer**](TransfersApi.md#complete_transfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**create_transfer**](TransfersApi.md#create_transfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
[**delete_transfer**](TransfersApi.md#delete_transfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**get_transfer**](TransfersApi.md#get_transfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**list_transfers**](TransfersApi.md#list_transfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
[**update_transfer**](TransfersApi.md#update_transfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information


# **complete_transfer**
> Transfer complete_transfer(account_id, transfer_id)

Complete a transfer

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import transfers_api
from mobwa.model.transfer import Transfer
from mobwa.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transfers_api.TransfersApi(api_client)
    account_id = "accountId_example" # str | The id of the account
    transfer_id = "transferId_example" # str | The id of the transfer to operate on

    # example passing only required values which don't have defaults set
    try:
        # Complete a transfer
        api_response = api_instance.complete_transfer(account_id, transfer_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling TransfersApi->complete_transfer: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |
 **transfer_id** | **str**| The id of the transfer to operate on |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_transfer**
> Transfer create_transfer(account_id)

Create a transfer

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import transfers_api
from mobwa.model.transfer import Transfer
from mobwa.model.create_transfer_request import CreateTransferRequest
from mobwa.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transfers_api.TransfersApi(api_client)
    account_id = "accountId_example" # str | The id of the account
    create_transfer_request = CreateTransferRequest(
        message="message_example",
        amount=1,
        currency="SGD",
        auto_complete=True,
        destination="destination_example",
    ) # CreateTransferRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Create a transfer
        api_response = api_instance.create_transfer(account_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling TransfersApi->create_transfer: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a transfer
        api_response = api_instance.create_transfer(account_id, create_transfer_request=create_transfer_request)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling TransfersApi->create_transfer: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |
 **create_transfer_request** | [**CreateTransferRequest**](CreateTransferRequest.md)|  | [optional]

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Transfer successfully created |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_transfer**
> Transfer delete_transfer(account_id, transfer_id)

Delete a transfer

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import transfers_api
from mobwa.model.transfer import Transfer
from mobwa.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transfers_api.TransfersApi(api_client)
    account_id = "accountId_example" # str | The id of the account
    transfer_id = "transferId_example" # str | The id of the transfers to operate on

    # example passing only required values which don't have defaults set
    try:
        # Delete a transfer
        api_response = api_instance.delete_transfer(account_id, transfer_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling TransfersApi->delete_transfer: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |
 **transfer_id** | **str**| The id of the transfers to operate on |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Null response |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transfer**
> Transfer get_transfer(account_id, transfer_id)

Retrieve information for a specific transfer

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import transfers_api
from mobwa.model.transfer import Transfer
from mobwa.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transfers_api.TransfersApi(api_client)
    account_id = "accountId_example" # str | The id of the account
    transfer_id = "transferId_example" # str | The id of the transfers to operate on

    # example passing only required values which don't have defaults set
    try:
        # Retrieve information for a specific transfer
        api_response = api_instance.get_transfer(account_id, transfer_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling TransfersApi->get_transfer: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |
 **transfer_id** | **str**| The id of the transfers to operate on |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_transfers**
> ListTransfersResponse list_transfers(account_id)

List all transfers related to the account

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import transfers_api
from mobwa.model.list_transfers_response import ListTransfersResponse
from mobwa.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transfers_api.TransfersApi(api_client)
    account_id = "accountId_example" # str | The id of the account

    # example passing only required values which don't have defaults set
    try:
        # List all transfers related to the account
        api_response = api_instance.list_transfers(account_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling TransfersApi->list_transfers: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |

### Return type

[**ListTransfersResponse**](ListTransfersResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A list of transfers related to the the account |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_transfer**
> Transfer update_transfer(account_id, transfer_id)

Change transfer information

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import transfers_api
from mobwa.model.transfer import Transfer
from mobwa.model.update_transfer_request import UpdateTransferRequest
from mobwa.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transfers_api.TransfersApi(api_client)
    account_id = "accountId_example" # str | The id of the account
    transfer_id = "transferId_example" # str | The id of the transfers to operate on
    update_transfer_request = UpdateTransferRequest(
        message="message_example",
        amount=1,
    ) # UpdateTransferRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Change transfer information
        api_response = api_instance.update_transfer(account_id, transfer_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling TransfersApi->update_transfer: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Change transfer information
        api_response = api_instance.update_transfer(account_id, transfer_id, update_transfer_request=update_transfer_request)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling TransfersApi->update_transfer: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |
 **transfer_id** | **str**| The id of the transfers to operate on |
 **update_transfer_request** | [**UpdateTransferRequest**](UpdateTransferRequest.md)|  | [optional]

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Null response |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

