# mobwa.AccountsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_account**](AccountsApi.md#create_account) | **POST** /accounts | Create an account
[**get_account**](AccountsApi.md#get_account) | **GET** /accounts/{accountId} | Get account information
[**list_account**](AccountsApi.md#list_account) | **GET** /accounts | List all accounts
[**recharge_account**](AccountsApi.md#recharge_account) | **POST** /accounts/{accountId}/recharge | Recharge the account
[**update_account**](AccountsApi.md#update_account) | **PUT** /accounts/{accountId} | Update account information
[**withdraw_money**](AccountsApi.md#withdraw_money) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account


# **create_account**
> ListTransfersResponse create_account()

Create an account

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import accounts_api
from mobwa.model.list_transfers_response import ListTransfersResponse
from mobwa.model.error import Error
from mobwa.model.create_account_request import CreateAccountRequest
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = accounts_api.AccountsApi(api_client)
    create_account_request = CreateAccountRequest(
        name="name_example",
    ) # CreateAccountRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create an account
        api_response = api_instance.create_account(create_account_request=create_account_request)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->create_account: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_account_request** | [**CreateAccountRequest**](CreateAccountRequest.md)|  | [optional]

### Return type

[**ListTransfersResponse**](ListTransfersResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A paged array of pets |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account**
> Account get_account(account_id)

Get account information

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import accounts_api
from mobwa.model.error import Error
from mobwa.model.account import Account
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = accounts_api.AccountsApi(api_client)
    account_id = "accountId_example" # str | The id of the account

    # example passing only required values which don't have defaults set
    try:
        # Get account information
        api_response = api_instance.get_account(account_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->get_account: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_account**
> ListAccountsResponse list_account()

List all accounts

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import accounts_api
from mobwa.model.error import Error
from mobwa.model.list_accounts_response import ListAccountsResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = accounts_api.AccountsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List all accounts
        api_response = api_instance.list_account()
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->list_account: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**ListAccountsResponse**](ListAccountsResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A paged array of pets |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recharge_account**
> Account recharge_account(account_id)

Recharge the account

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import accounts_api
from mobwa.model.recharge_account_request import RechargeAccountRequest
from mobwa.model.error import Error
from mobwa.model.account import Account
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = accounts_api.AccountsApi(api_client)
    account_id = "accountId_example" # str | The id of the account
    recharge_account_request = RechargeAccountRequest(
        amout=1,
    ) # RechargeAccountRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Recharge the account
        api_response = api_instance.recharge_account(account_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->recharge_account: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Recharge the account
        api_response = api_instance.recharge_account(account_id, recharge_account_request=recharge_account_request)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->recharge_account: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |
 **recharge_account_request** | [**RechargeAccountRequest**](RechargeAccountRequest.md)|  | [optional]

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_account**
> Account update_account(account_id)

Update account information

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import accounts_api
from mobwa.model.update_account_request import UpdateAccountRequest
from mobwa.model.error import Error
from mobwa.model.account import Account
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = accounts_api.AccountsApi(api_client)
    account_id = "accountId_example" # str | The id of the account
    update_account_request = UpdateAccountRequest(
        name="name_example",
    ) # UpdateAccountRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update account information
        api_response = api_instance.update_account(account_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->update_account: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update account information
        api_response = api_instance.update_account(account_id, update_account_request=update_account_request)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->update_account: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |
 **update_account_request** | [**UpdateAccountRequest**](UpdateAccountRequest.md)|  | [optional]

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **withdraw_money**
> Account withdraw_money(account_id)

Withdraw money from the account

### Example

* Basic Authentication (basicAuth):
```python
import time
import mobwa
from mobwa.api import accounts_api
from mobwa.model.recharge_account_request import RechargeAccountRequest
from mobwa.model.error import Error
from mobwa.model.account import Account
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = mobwa.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: basicAuth
configuration = mobwa.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with mobwa.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = accounts_api.AccountsApi(api_client)
    account_id = "accountId_example" # str | The id of the account
    body = RechargeAccountRequest(
        amout=1,
    ) # RechargeAccountRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Withdraw money from the account
        api_response = api_instance.withdraw_money(account_id)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->withdraw_money: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Withdraw money from the account
        api_response = api_instance.withdraw_money(account_id, body=body)
        pprint(api_response)
    except mobwa.ApiException as e:
        print("Exception when calling AccountsApi->withdraw_money: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| The id of the account |
 **body** | [**RechargeAccountRequest**](RechargeAccountRequest.md)|  | [optional]

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Expected response to a valid request |  -  |
**0** | unexpected error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

