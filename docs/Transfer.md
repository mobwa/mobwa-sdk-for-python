# Transfer


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**message** | **str** |  | [optional] 
**amount** | **int** |  | [optional] 
**currency** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**auto_complete** | **bool** |  | [optional] 
**source** | [**TransferSource**](TransferSource.md) |  | [optional] 
**destination** | [**TransferSource**](TransferSource.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


