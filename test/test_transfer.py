"""
    Mobwa Payments Hub

    Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD)   # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import mobwa
from mobwa.model.transfer_source import TransferSource
globals()['TransferSource'] = TransferSource
from mobwa.model.transfer import Transfer


class TestTransfer(unittest.TestCase):
    """Transfer unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testTransfer(self):
        """Test Transfer"""
        # FIXME: construct object with mandatory attributes with example values
        # model = Transfer()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
