"""
    Mobwa Payments Hub

    Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD)   # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import mobwa
from mobwa.model.user import User
globals()['User'] = User
from mobwa.model.list_users_response import ListUsersResponse


class TestListUsersResponse(unittest.TestCase):
    """ListUsersResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testListUsersResponse(self):
        """Test ListUsersResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = ListUsersResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
