"""
    Mobwa Payments Hub

    Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD)   # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import mobwa
from mobwa.model.complete_transfer_response import CompleteTransferResponse


class TestCompleteTransferResponse(unittest.TestCase):
    """CompleteTransferResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCompleteTransferResponse(self):
        """Test CompleteTransferResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = CompleteTransferResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
